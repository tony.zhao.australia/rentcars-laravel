<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Repositories\CarsRepository;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\Log;

use Validator;

class CheckoutController extends Controller
{
    /**
     * @var CarsRepository
     */
    private $carsRepository;
    private $orderRepository;

    public function __construct(CarsRepository $carsRepository, OrderRepository $orderRepository)
    {
        $this->carsRepository = $carsRepository;
        $this->orderRepository = $orderRepository;

        $this->middleware('auth:api', ['except' => []]);
    }

    //check out a car
    function addCheckoutCar(Request $request){

        //validate the user's input
        $validator = Validator::make($request->all(), [
            'makeId' => 'required|numeric|min:1|max:1000000',
            'checkoutType' => 'numeric',
            'customerName' => 'string',
            'checkoutDate' => 'required|date'
        ]);

        if ($validator->fails()) {
            return response()->json(config('constants.errors.USER_INPUT_ERROR_10007'));
        }

        $makeId = $request->input('makeId');
        $checkoutType = $request->input('checkoutType');
        $customerName = $request->input('customerName');
        $checkoutDate = $request->input('checkoutDate');

        //This must be a logged in user
        $userInfo = auth()->user();
        if(empty($userInfo)){
            return response()->json(config('constants.errors.MUST_LOGIN_10003'));
        }

        //check if this car has been check out
        $carAvailability = $this->carsRepository->checkCarAvailability($makeId);

        if(!$carAvailability){
            return response()->json(config('constants.errors.CAR_NOT_AVAILABLE_10001'));
        }

        $orderData = [
            'make_id' => $makeId,
            'user_id' => $userInfo['id'],
            'customer_name' => $customerName,
            'checkout_date' => $checkoutDate,
            'checkout_type' => $checkoutType,
            'status' => 1,
        ];

        DB::beginTransaction();
        try {
            $this->orderRepository->createCheckoutOrder($orderData);
            $this->carsRepository->updateCarAvailability($makeId, 0);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::warning(__FILE__.':Line No.'.__LINE__.'---ERROR_WHEN_CREATE_AN_ORDER_10002---'.$e->getMessage());
            return response()->json(config('constants.errors.ERROR_WHEN_CREATE_AN_ORDER_10002'));
        }

        $resReturn = [
            'success' => 'yes',
            'data' => [
                'makeId' => $makeId,
                'status' => 0,
                'checkoutType' => $checkoutType
            ]
        ];
        return response()->json($resReturn);
    }

    //check back a car
    function addCheckbackCar(Request $request){

        //validate the user's input
        $validator = Validator::make($request->all(), [
            'makeId' => 'required|numeric|min:1|max:1000000',
            'checkbackCondition' => 'numeric',
            'checkbackAt' => 'required|date'
        ]);

        if ($validator->fails()) {
            return response()->json(config('constants.errors.USER_INPUT_ERROR_10007'));
        }

        $makeId = $request->input('makeId');
        $checkbackAt = $request->input('checkbackAt');
        $checkbackCondition = $request->input('checkbackCondition');
        $userInfo = auth()->user();

        if(empty($userInfo)){
            return response()->json(config('constants.errors.MUST_LOGIN_10003'));
        }

        //check if this user is allowed to checkback this car
        $orderToBeCheckback = $this->orderRepository->getOrderFromMakeIdAndUserId($makeId, $userInfo->id);
        if(empty($orderToBeCheckback)){
            return response()->json(config('constants.errors.NOT_ALLOWED_CHECKBACK_CAR_10004'));
        }

        //update the order info and mark the car available.
        DB::beginTransaction();
        try {
            $updateOrderData = [
                'status' => 2,
                'checkback_condition' => $checkbackCondition,
                'checkback_at' => $checkbackAt
            ];
            $this->orderRepository->checkbackCar($orderToBeCheckback, $updateOrderData);
            $this->carsRepository->updateCarAvailability($makeId, 1);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::warning(__FILE__.':Line No.'.__LINE__.'---ERROR_WHEN_UPDATE_AN_ORDER_10005---'.$e->getMessage());
            return response()->json(config('constants.errors.ERROR_WHEN_UPDATE_AN_ORDER_10005'));
        }

        $resReturn = [
            'success' => 'yes',
            'data' => [
                'makeId' => $makeId,
                'status' => 1,
            ]
        ];
        return response()->json($resReturn);

    }
}
