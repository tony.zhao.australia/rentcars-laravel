<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Repositories\CarsRepository;
use App\Repositories\OrderRepository;
use App\Services\ImportCarsService;
use Illuminate\Support\Facades\Log;

class CarController extends Controller
{

    private $carsRepository;
    private $orderRepository;
    private $importCarsService;

    //Inject Repository and Service into __construct
    public function __construct(CarsRepository $carsRepository,
                                OrderRepository $orderRepository,
                                ImportCarsService $importCarsService
    ){
        $this->carsRepository = $carsRepository;
        $this->orderRepository = $orderRepository;
        $this->importCarsService = $importCarsService;
    }

    //get all the cars
    public function carslist(){
       $carList = $this->carsRepository->getCarlist();
       $userInfo = auth()->user();

       $resReturn = [];
        foreach ($carList as  $key=>$car){

            //if_owner==true means this user has checkout this car, so in the front end,  he can checkback this car
            //if_owner==false means this user never checkout this car, so this user can checkout this car
            if(!empty($userInfo)){
                $orderInfo = $this->orderRepository->getOrderFromMakeIdAndUserId($car->make_id, $userInfo->id);
                $carList[$key]->if_owner = !empty($orderInfo);

            }else{
                $carList[$key]->if_owner = false;
            }

            $resReturn['data'][] = $carList[$key];

        }

        return response()->json($resReturn);
    }

    //This method is only used once, then can be deleted
    //It calls an injected service to revoke a third party api and then import data into DB
    public function importcars(){

        try {
            $this->importCarsService->importcars();
        }catch (\Exception $e){
            Log::warning(__FILE__.':Line No.'.__LINE__.'---ERROR_WHEN_CALL_API_TO_GET_CARS_10006---'.$e->getMessage());
            return response()->json([
                'success' => 'no'
            ]);
        }

        return response()->json([
            'success' => 'yes'
        ]);
    }
}
