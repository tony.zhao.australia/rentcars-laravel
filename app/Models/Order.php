<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'rc_orders';

    protected $fillable = ['make_id', 'user_id', 'customer_name', 'checkout_date', 'checkout_type', 'checkback_at', 'checkback_condition', 'status'];


}
