<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ImportCarsService{
    //this function is used to import data from api and insert into database
    //this is a one-off function.
    public function importcars(){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://vpic.nhtsa.dot.gov/api/vehicles/GetMakesForVehicleType/car?format=json',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
            ),
        ));

        try {
            $response = curl_exec($curl);
        }catch (\Exception $e){
            Log::warning(__FILE__.':Line No.'.__LINE__.'---ERROR_WHEN_CALL_API_TO_GET_CARS_10006---'.$e->getMessage());
            throw $e;
        }

        $res = json_decode($response,true);

        if($res['Count']>0 && !empty($res['Results'])){

            $insertData = [];

            //loop the results from api and equip to an array that fit the table rc_cars
            foreach($res['Results'] as $singleCar){
                $insertData[] = [
                    'make_id' => $singleCar['MakeId'],
                    'make_name' => $singleCar['MakeName'],
                    'vehicle_type_id' => $singleCar['VehicleTypeId'],
                ];
            }

            DB::table('rc_cars')->insertOrIgnore($insertData);

        }

        curl_close($curl);
        return $res;

    }
}
