<?php
namespace App\Repositories;
use Illuminate\Support\Facades\DB;
use App\Models\Car;

class CarsRepository{

    public function getCarlist(){
        $res = DB::table('rc_cars')
            ->join('rc_cars_type', 'rc_cars.vehicle_type_id', '=', 'rc_cars_type.id')
            ->select('rc_cars.*', 'rc_cars_type.type_name')->paginate(20);

        return $res;
    }

    //$status=1 available    $status=0 not available
    public function checkCarAvailability($makeId)
    {
       $thisCar = Car::where('make_id', $makeId)->first();
        if(empty($thisCar)){
            return false;
        }
       return $thisCar->status == 1;
    }

    //$status=1 available    $status=0 not available
    public function updateCarAvailability($makeId, $status=1){

       return Car::where('make_id', $makeId)
            ->update(['status' => $status]);

    }
}
