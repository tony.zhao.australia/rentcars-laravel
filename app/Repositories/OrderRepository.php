<?php
namespace App\Repositories;
use App\Models\Order;

class OrderRepository{

    public function createCheckoutOrder($orderData){

       return Order::create($orderData);
    }

    //to check if this user has checkout this car
    public function getOrderFromMakeIdAndUserId($makeId, $userId){
        return Order::where('make_id', $makeId)
            ->where('user_id', $userId)
            ->where('status', 1)
            ->first();
    }

    public function checkbackCar($orderToBeCheckback, $updateOrderData){
       return $orderToBeCheckback->update($updateOrderData);
    }
}
