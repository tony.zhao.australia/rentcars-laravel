<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_login()
    {
        $response = $this->postJson('/api/auth/login', [
            'email' => 'tony.zhao.australia@gmail.com',
            'password' => '123456'
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'token_type' => 'bearer',
            ]);
    }
}
