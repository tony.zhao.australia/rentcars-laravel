<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Car;

class OrderTest extends TestCase
{
    protected $requestDataCanCheckout;
    protected $requestDataCanNotCheckout;

    protected function setUp(): void
    {
        parent::setUp();
        $this->requestDataCanCheckout = [
            "makeId" => 440,
            "checkoutType" => 1,
            "customerName" => "Jack Name",
            "checkoutDate" => "2021-11-01"

        ];

        $this->requestDataCanNotCheckout = [
            "makeId" => 441,
            "checkoutType" => 1,
            "customerName" => "Jack Name",
            "checkoutDate" => "2021-11-01"

        ];
    }

    public function test_checkout_car_not_login()
    {
        $response = $this->postJson('/api/auth/checkoutCar', $this->requestDataCanCheckout);

        $response->assertStatus(401);
    }

    public function test_checkout_car_with_login()
    {
        $this->postJson('/api/auth/login', [
            'email' => 'tony.zhao.australia@gmail.com',
            'password' => '123456'
        ]);

        $response = $this->postJson('/api/auth/checkoutCar', $this->requestDataCanCheckout);
        $response->assertStatus(200)->assertJson([
            'success' => 'yes',
        ]);

        $response = $this->postJson('/api/auth/checkoutCar', $this->requestDataCanNotCheckout);
        $response->assertStatus(200)->assertJson([
            'success' => 'no',
        ])->assertJsonFragment(["errorNo"=> 10001]);
    }

    function cleanData(){
        Car::where('make_id', 440)
            ->update(['status' => 1]);
    }

    public function tearDown():void
    {
        $this->cleanData();

        parent::tearDown();

    }

}
