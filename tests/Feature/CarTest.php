<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CarTest extends TestCase
{
    public function test_carlist_api()
    {

        $this->withoutExceptionHandling();
        $response = $this->json('GET', '/api/cars?page=1');

        $response->assertStatus(200);
        $response->assertJsonFragment(["make_id" => 440]);
        $response->assertJsonCount(20, $key = 'data');

    }

}
