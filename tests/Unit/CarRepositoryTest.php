<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use App\Repositories\CarsRepository;
use Tests\TestCase;

class CarRepositoryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_getCarlist()
    {
        $repository = $this->app->make(CarsRepository::class);
        $carlist = $repository->getCarlist();
        $this->assertSame(20, count($carlist), 'car list count is not 20');


    }
}
