
## Structure
### 1 Controllers:

(1)/app/Http/Controllers/CarController.php
<br>
to get carlist and import car from 3rd-party api

<br>
(2) /app/Http/Controllers/CheckoutController
<br>
to checkout and checkback cars
<br>
<br>
(3) /app/Http/Controllers/AuthController
<br>
for authentication
<br>

### 2 Repositories
(1)app/Repositories/CarsRepository.php
<br>
interact with table rc_cars
<br>
<br>
(2)app/Repositories/OrderRepository.php
<br>
to interact with table rc_orders
<br>

### 3 Services
(1)app/Services/ImportCarsService.php
<br>
to revoke 3rd-party api and import data
<br>

### 4 Models
(1)app/Models/Car.php
<br>
ORM for Car
<br>
<br>
(2)app/Models/Order.php
<br>
ORM for Order
<br>
<br>
(3)app/Models/User.php
<br>
ORM for User
<br>
<br>

## DB Migration

2021_11_08_041553_create_rc_cars.php
<br>
2021_11_08_042125_create_rc_cars_type.php
<br>
2021_11_08_042511_create_rc_orders.php
<br>


<br>
please run
<br>
php artisan migrate


## Test

tests/Feature/CarTest.php
<br>
tests/Feature/OrderTest.php
<br>
tests/Feature/UserTest.php

<br>
Note:  please see phpunit.xml
<br>
`server name="DB_CONNECTION" value="mysql_test"`

<br>
So, please also setup a "mysql_test" DB config in your config/database.php
<br>
please run
<br>
php artisan test

## To be finished:
1  CI/CD
<br>
2  Pagination


