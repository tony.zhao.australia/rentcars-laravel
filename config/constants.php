<?php

return [
    'errors' => [
        'CAR_NOT_AVAILABLE_10001' => [
            'success' => 'no',
            'msg' => [
                'errorNo' => 10001,
                'errorMsg' => 'This car is not available'
            ]
        ],
        'ERROR_WHEN_CREATE_AN_ORDER_10002' => [
            'success' => 'no',
            'msg' => [
                'errorNo' => 10002,
                'errorMsg' => 'Error when create an order'
            ]
        ],
        'MUST_LOGIN_10003' => [
            'success' => 'no',
            'msg' => [
                'errorNo' => 10003,
                'errorMsg' => 'You must login'
            ]
        ],
        'NOT_ALLOWED_CHECKBACK_CAR_10004' => [
            'success' => 'no',
            'msg' => [
                'errorNo' => 10004,
                'errorMsg' => 'You are not allowed to checkback this car'
            ]
        ],
        'ERROR_WHEN_UPDATE_AN_ORDER_10005' => [
            'success' => 'no',
            'msg' => [
                'errorNo' => 10005,
                'errorMsg' => 'Error when update an order'
            ]
        ],
        'ERROR_WHEN_CALL_API_TO_GET_CARS_10006' => [
            'success' => 'no',
            'msg' => [
                'errorNo' => 10006,
                'errorMsg' => 'Error when call api to get cars'
            ]
        ],
        'USER_INPUT_ERROR_10007' => [
            'success' => 'no',
            'msg' => [
                'errorNo' => 10007,
                'errorMsg' => 'User input error'
            ]
        ],
    ]
];
