<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("CREATE TABLE IF NOT EXISTS `rc_cars` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `make_id` int(11) unsigned NOT NULL DEFAULT '1',
 `make_name` varchar(111) NOT NULL DEFAULT '',
 `vehicle_type_id` tinyint(1) unsigned NOT NULL DEFAULT '2',
 `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 available 0 NA',
 `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8mb4");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_cars');
    }
}
