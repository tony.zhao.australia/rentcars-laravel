<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcCarsType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TABLE `rc_cars_type` (
 `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
 `type_name` varchar(111) NOT NULL DEFAULT '',
 `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1-active; 0-inactive',
 `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4");

        DB::statement("INSERT INTO `rc_cars_type` (`id`, `type_name`, `status`, `created_at`, `updated_at`) VALUES
    (2, 'Passenger Car', 1, '2021-11-01 14:39:55', '2021-11-01 14:39:55');");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_cars_type');
    }
}
