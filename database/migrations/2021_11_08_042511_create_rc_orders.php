<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TABLE `rc_orders` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `make_id` int(11) unsigned NOT NULL DEFAULT '1',
 `user_id` bigint(20) unsigned NOT NULL DEFAULT '1',
 `customer_name` varchar(111) NOT NULL,
 `checkout_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `checkout_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 loan 2 test drive',
 `checkback_at` datetime DEFAULT NULL,
 `checkback_condition` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1 good 2 normal 3 bad 0 na',
 `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 checkout 2 checkback',
 `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_orders');
    }
}
